## SSH Login

```
ssh pi@ip_address
```

## Copy file

```
scp file.txt pi@ip_address:~/path
```
