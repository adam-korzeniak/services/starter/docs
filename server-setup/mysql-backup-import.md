1. Dump database

```
mysqldump -u root -p --all-databases > alldb.sql
```

2. Import databases

```
mysql -u root -p < alldb.sql
```
