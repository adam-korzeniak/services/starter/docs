## Setup Raspberry Server

### Install OS to SD Card

1. Download PI OS and Raspberry Pi Imager

https://www.raspberrypi.org/software/operating-systems/

https://www.raspberrypi.org/software/

2. Upload OS to SD Card using Pi Imager

### Setup Raspberry

1. Insert SD Card to Raspberry

2. Provide default credentials:

```
username: pi
password: raspberry
```

3. Change password

```
passwd
```

4. Configure raspberry pi by running a following command:

```
sudo raspi-config
```

5. Choose following options:

- System Options -> Hostname:
    - Change hostname to ```adampi```
- System Options -> Wireless Lan
- System Options -> Boot Options -> Network at boot
- Interfacing Options -> SSH
- Update

6. Reboot

```
reboot
```

7. Update software package by running following commands:

```
sudo apt-get update
sudo apt-get dist-upgrade
sudo apt autoremove
```

8. Check your ip address:

```
sudo ip addr show
```

9. Setup automatic updates

```
sudo su
crontab -e
0 9 * * 6 apt-get update && apt-get dist-upgrade -y
```

10. Reboot

```
reboot
```

11. Update router port forwarding


