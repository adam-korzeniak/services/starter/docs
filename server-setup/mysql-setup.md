## MySQL Setup

### Download MySQL

1. Install MySQL Server

```
sudo apt-get install mariadb-server
```

2. Login to database

```
sudo mysql
```

3. Create Database

```
SET PASSWORD FOR 'root'@'localhost' = PASSWORD('ROOT_PASSWORD');
CREATE USER 'USER_NAME'@'localhost' IDENTIFIED BY 'USER_PASSWORD';
CREATE USER 'USER_NAME'@'%' IDENTIFIED BY 'USER_PASSWORD';
```

4. Grant privileges to databases

```
GRANT ALL PRIVILEGES ON *.* TO 'USER_NAME'@'localhost' IDENTIFIED BY 'USER_PASSWORD';
GRANT ALL PRIVILEGES ON *.* TO 'USER_NAME'@'%' IDENTIFIED BY 'USER_PASSWORD';
FLUSH PRIVILEGES;
SELECT Host, User FROM mysql.user
```

5. Enable external access

```
sudo nano /etc/mysql/mariadb.conf.d/50-server.cnf
```

Add comment before ‘bind-address’

```
#bind-address = 127.0.0.1
```

6. Change mysql default port for security

```
sudo nano /etc/mysql/mariadb.conf.d/50-server.cnf
```

Edit ‘port’ value

```
port = XXXX
```

7. Restart mysql

```
systemctl restart mysql.service
```
