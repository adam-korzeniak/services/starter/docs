## Encrypt

```
encrypt.sh input=PROPERTY_VALUE password=ENCRYPTION_KEY
```

## Decrypt

```
decrypt.sh input=ENCRYPTED_PROPERTY_VALUE password=ENCRYPTION_KEY
```
