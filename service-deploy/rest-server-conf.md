# Setup REST Service

1. Create file for services

```
sudo touch /etc/systemd/system/micro-xxx.service
```

2. Edit file and put following content:

```
[Unit]
Description=Services - Crypto
After=syslog.target

[Service]
User=pi
ExecStart=/usr/bin/java -Denv=uat -Djasypt.encryptor.password=ENCRYPTIONKEY -jar /var/my-apps/services/crypto.jar
SuccessExitStatus=143

[Install]
WantedBy=multi-user.target
```

3. Add service to autostart

```
sudo systemctl enable micro-xxx.service
```

4. Start service

```
sudo systemctl start micro-xxx.service
```
