1. Change directory to service

```
cd C:\Home\Development\Repos\services\services\crypto
```

2. Run build

```
mvn clean package
```

3. Copy application to server

```
scp target/crypto*.jar pi@ip_address:~/home/pi/server/app
```

4. SSH into server

```
ssh pi@ip_address
```

5. Copy file to service directory

```
sudo cp /home/pi/server/app/crypto*.jar /var/my-apps/services/crypto.jar
sudo cp /home/pi/server/app/crypto*.jar /var/my-apps/services/crypto.jar
```

6. Restart service

```
sudo systemctl restart micro-xxx.service
```
